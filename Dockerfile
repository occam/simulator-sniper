# DOCKER-VERSION 0.11.1
# LOCAL BUILD ID 12345678-1234-1234-1234-1234567890ab
FROM occam/base-66249f7e-9478-11e4-ac02-001fd05bb228
ADD . /occam/simulator-707e0416-a372-11e4-9c8a-001fd05bb228
RUN cd /occam/simulator-707e0416-a372-11e4-9c8a-001fd05bb228; /bin/bash -c 'source /occam/setup_python.sh; source /occam/setup_perl.sh; cd /occam/simulator-707e0416-a372-11e4-9c8a-001fd05bb228; bash /occam/simulator-707e0416-a372-11e4-9c8a-001fd05bb228/build.sh'
VOLUME ["/occam/simulator-707e0416-a372-11e4-9c8a-001fd05bb228"]
CMD cd /occam/simulator-707e0416-a372-11e4-9c8a-001fd05bb228; /bin/bash -c 'source /occam/setup_python.sh; source /occam/setup_perl.sh; cd /job;  pyenv local 2.7.6; python /occam/simulator-707e0416-a372-11e4-9c8a-001fd05bb228/launch.py'
