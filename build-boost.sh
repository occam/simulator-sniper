BOOST_VERSION=1.55.0 # Boost. I hate it.
BOOST_BUILDER=b2     # Boost 1.5+ uses 'b2' and else 'bjam'

BOOST_VER_UNDERSCORE=${BOOST_VERSION//./_}

# Download boost
wget -Nc http://sourceforge.net/projects/boost/files/boost/${BOOST_VERSION}/boost_${BOOST_VER_UNDERSCORE}.tar.bz2/download -O boost_${BOOST_VER_UNDERSCORE}.tar.bz2 || exit
# Arguments:
# wget -N (download if newer than existing file) -c (resume partially downloaded file)
tar -xvf boost_${BOOST_VER_UNDERSCORE}.tar.bz2 || exit

# Build boost (It fails to build with newer versions, so we will patch later)
cd boost_${BOOST_VER_UNDERSCORE}
./bootstrap.sh || exit

./${BOOST_BUILDER} || exit
cd .. # sniper root

# Patch boost (it fails string -Werror when included! Chrimity!)

# Patched files:
# /tuple/detail/tuple_basic.hpp
# /spirit/home/classic/tree/parse_tree.hpp
# /spirit/home/classic/core/primitives/primitives.hpp
# /spirit/home/classic/core/non_terminal/impl/grammar.ipp

cp occam/boost_1_55_0.patch .
patch -p0 < boost_1_55_0.patch
