# Sniper expects to see pin source in 'pin_kit'
# You can override with PIN_HOME but this is easier:
if [ ! -d pin_kit ]
then
  # Get pin 2.13rev61206
  wget -Nc http://software.intel.com/sites/landingpage/pintool/downloads/pin-2.13-61206-gcc.4.4.7-linux.tar.gz || exit
  tar -xvf pin-2.13-61206-gcc.4.4.7-linux.tar.gz || exit

  mv pin-2.13-61206-gcc.4.4.7-linux pin_kit
fi
