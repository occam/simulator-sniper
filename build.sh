bash build-ubuntu.sh

cd package
pyenv local 2.7.6

pip install sqlite3

# sniper root
bash ../build-pin.sh

# Build sniper/pin
make || exit

# Build test program(s)
cd test

# fft
cd fft
make fft
cd .. # sniper-root/test

cd fft-hetero
make fft
cd .. # sniper-root/test

cd fft-marker
make fft
cd .. # sniper-root/test

cd .. # sniper-root

# Go back into occam scripts path
cd ..
