import json

input_file = open('input.cfg', 'w')

# Reformatted options
options = {}

def write_dict(title, d):
  # Write [header]
  if not title == None:
    input_file.write("[%s]\n" % (title))

  for key, value in d.items():
    if isinstance(value, dict):
      ""
    elif isinstance(value, (bool)):
      if value == True:
        input_file.write('%s = "true"\n' % (key))
      else:
        input_file.write('%s = "false"\n' % (key))
    elif isinstance(value, (int, long, float)):
      input_file.write("%s = %s\n" % (key, value))
    else:
      input_file.write('%s = "%s"\n' % (key, value))

  for key, value in d.items():
    if isinstance(value, dict):
      write_dict(key, value)

def out_dict(title, d, to):
  # Write [header]
  if not title is None:
    to[title] = {}
    to = to[title]

  # Write items
  for k, v in d.items():
    if not isinstance(v, dict):
      key = k
      value = v

      # Rewrite values if needed:

      # Special case for network/emesh_hop_by_hop/dimensions
      if key == "dimensions":
        if v == "Line/Ring":
          value = 1
        elif v == "2D Mesh/Torus":
          value = 2
        else:
          value = 0
      elif key == "writethrough":
        value = (v == 1)

      # Rewrite and redo keys if needed:

      # Common case: just write the value
      to[key] = value

  # Write sub-items
  for k, v in d.items():
    if isinstance(v, dict):
      if k == "Hook Periodic Instructions":
        k = "hook_periodic_ins"
      elif k == "OS Emulation":
        k = "osemu"
      elif k == "Performance Model":
        k = "perf_model"
      elif k == "Last-Level Cache":
        k = "llc"

      if k == "Global":
        # Special case for global (they go to global space and not
        #                          under a header)
        out_dict(None, v, to)
      else:
        new_title = k.lower().replace(' ', '_').replace('-', '')
        if not title is None:
          new_title = "%s/%s" % (title, new_title)

        out_dict(new_title, v, to)

# Read general configuration
data = json.load(open('config.json'))
out_dict(None, data, options)

# Read CPU config
data = json.load(open('cpu.json'))
out_dict(None, data, options)

write_dict(None, options)
