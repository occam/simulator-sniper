{
  "General": {
    "enable_icache_modeling": {
      "label": "Enable I-Cache Modeling",
      "type": "boolean",
      "default": true
    },

    "enable_syscall_emulation": {
      "label": "Enable SYSCALL Emulation",
      "type": "boolean",
      "default": true
    },

    "total_cores": {
      "label": "Number of Cores",
      "type": "int",
      "description": "The number of cores to simulate.",
      "default": 2
    },

    "magic": {
      "label": "Wait for ROI Begin/End",
      "type": "boolean",
      "description": "Whether or not to wait for the Roi{Begin,End} magic instruction. Otherwise, just start the performance simulation straight away. The Roi instructions are typically integrated into specific benchmarks and are used to limit the simulation to useful areas of the program's executation. For instance, to ignore initialization.",
      "default": false
    },

    "roi_script": {
      "label": "Allow ROI Script Override",
      "type": "boolean",
      "description": "Whether or not to allow a script to override and thus ignore any ROI Begin/End instructions built into the benchmarks. The Roi instructions are typically integrated into specific benchmarks and are used to limit the simulation to useful areas of the program's executation. For instance, to ignore initialization.",
      "default": false
    },

    "inst_mode_init": {
      "label": "Instrumentation Mode (Initialization)",
      "type": ["detailed", "cache_only", "fast_forward"],
      "description": "",
      "default": "cache_only"
    },

    "inst_mode_roi": {
      "label": "Instrumentation Mode (ROI Window)",
      "type": ["detailed", "cache_only", "fast_forward"],
      "description": "",
      "default": "cache_only"
    },

    "inst_mode_end": {
      "label": "Instrumentation Mode (End)",
      "type": ["detailed", "cache_only", "fast_forward"],
      "description": "",
      "default": "cache_only"
    },

    "inst_mode_output": {
      "label": "Instrumentation Mode Output",
      "type": "boolean",
      "description": "Whether or not the setting of the instrumentation mode is logged in the simulator output.",
      "default": true
    },

    "syntax": {
      "label": "Disassembly Syntax",
      "type": ["intel", "att", "xed"],
      "description": "The syntax to represent disassembly in any output.\n\n* **intel**: Intel syntax\n* **att**: AT&T (gas) syntax\n* **xed**: syntax given by the XED decoder",
      "default": "intel"
    },

    "enable_signals": {
      "label": "Enable Signals",
      "type": "boolean",
      "description": "When true, the Pin tool will not intercept UNIX signals for SIGILL, SIGSEGV and SIGFPE and Pin will thus instrument every instruction and not instrument with a block cache. When false, a block cache will be used.",
      "default": false
    },

    "enable_smc_support": {
      "label": "Enable SMC Support",
      "type": "boolean",
      "description": "Whether or not to support Self-Modifying Code (SMC). When this is true, Pin cannot cache its instrumentation and will not use a block cache. However, it will still instrument a block at a time, just throwing away the work.",
      "default": false
    },

    "enable_pinplay": {
      "label": "Enable PinPlay",
      "type": "boolean",
      "description": "Whether or not to run with a 'pinball' instead of an application.",
      "default": false
    },

    "suppress_stdout": {
      "label": "Suppress Standard Out",
      "type": "boolean",
      "description": "Whether or not to suppress the application's output to stdout.",
      "default": false
    },

    "suppress_stderr": {
      "label": "Suppress Standard Error",
      "type": "boolean",
      "description": "Whether or not to suppress the application's output to stderr",
      "default": false
    }
  },

  "Log": {
    "enabled": {
      "label": "Enable",
      "type": "boolean",
      "description": "Whether or not to enable logging.",
      "default": false
    },

    "stack_trace": {
      "label": "Log Stack Trace",
      "type": "boolean",
      "description": "Whether or not to log the stack trace. It does not seem to do anything.",
      "default": false
    },

    "mutex_trace": {
      "label": "Log Mutex Trace",
      "type": "boolean",
      "description": "Whether or not to log mutex updates. This will create a file called 'mutextrace.txt' which contains a log of all updates to the state of a mutex. Each line of the log is in the form of three integers 'A B C' where A is the cpu ID, B is the elapsed time in nanoseconds, and C is the mutex state.",
      "default": false
    },

    "pin_codecache_trace": {
      "label": "Pin Codecache Trace",
      "type": "boolean",
      "description": "Whether or not to log code cache trace and statistics. When true, this will create two files. 'cctrace.txt' logs the state of the code cache during execution. Each line is of the form 'A B C D E F G H I' where:\n\n* **A**: Time (nanoseconds)\n* **B**: Number of blocks created\n* **C**: Number of times the cache was full\n* **D**: Number of times the cache was flushed\n* **E**: Number of times the code cache was entered\n* **F**: Number of times a code block was linked\n* **G**: Number of times a code block was unlinked\n* **H**: Number of times a code block was invalidated\n* **I**: Number of times a code block was inserted into the code cache\n\n'ccstats.txt' contains a set of statistics relating to the implementation of the code cache. Each line is of the form 'A B C D E F G H I J' where:\n\n* **A**: Time (nanoseconds)\n* **B**: Code Memory reserved\n* **C**: Directory Memory Used\n* **D**: Code Memory Used\n* **E**: Exit Stub Bytes\n* **F**: Link Bytes\n* **G**: Cache Size Limit\n* **H**: Block Size\n* **I**: Number of traces in the cache\n* **J**: Number of Exit Stubs in the cache",
      "default": false
    }
  },

  "Trace Input": {
    "enabled": {
      "label": "Enabled",
      "description": "",
      "type": "boolean",
      "default": false
    },
    "address_randomization": {
      "label": "Enable Address Randomization",
      "description": "",
      "type": "boolean",
      "default": false
    },
    "stop_with_first_app": {
      "label": "Stop With First App",
      "type": "boolean",
      "default": true
    },
    "restart_apps": {
      "label": "Restart Apps",
      "type": "boolean",
      "default": false
    },
    "mirror_output": {
      "label": "Mirror the Output",
      "type": "boolean",
      "default": false
    },
    "num_runs": {
      "label": "Number of Runs",
      "type": "int",
      "units": "runs",
      "default": 1
    },
    "trace_prefix": {
      "label": "Trace Prefix",
      "type": "string",
      "default": ""
    }
  },

  "Scheduler": {
    "type": {
      "label": "Type",
      "type": ["pinned", "roaming", "static", "big small"],
      "default": "pinned"
    },

    "Pinned": {
      "quantum": {
        "label": "Quantum",
        "description": "Schedule quantum (round-robin for active threads on each core.)",
        "type": "int",
        "units": "nanoseconds",
        "default": 1000000
      },
      "core_mask": {
        "label": "Core Mask",
        "type": "int",
        "description": "Mask of cores on which threads can be schedules. Default is 1, which means all cores.",
        "default": 1
      },
      "interleaving": {
        "label": "Interleaving",
        "type": "int",
        "description": "The interleaving of round-robin initial assignment. For instance, 2 would be queuing cores like this: 0, 2, 4, 6, 1, 3, 5, 7.",
        "default": 1
      }
    },

    "Roaming": {
      "quantum": {
        "label": "Quantum",
        "description": "Schedule quantum (round-robin for active threads on each core.)",
        "type": "int",
        "units": "nanoseconds",
        "default": 1000000
      },
      "core_mask": {
        "label": "Core Mask",
        "type": "int",
        "description": "Mask of cores on which threads can be schedules. Default is 1, which means all cores.",
        "default": 1
      }
    },

    "Static": {
      "core_mask": {
        "label": "Core Mask",
        "type": "int",
        "description": "Mask of cores on which threads can be schedules. Default is 1, which means all cores.",
        "default": 1
      }
    },

    "Big Small": {
      "quantum": {
        "label": "Quantum",
        "description": "Schedule quantum (round-robin for active threads on each core.)",
        "type": "int",
        "units": "nanoseconds",
        "default": 1000000
      },
      "debug": {
        "label": "Enable Debug Logging",
        "type": "boolean",
        "description": "Whether or not debugging statements are logged.",
        "default": false
      }
    }
  },

  "DVFS": {
    "type": {
      "label": "Type",
      "type": ["simple"],
      "default": "simple"
    },
    "transition_latency": {
      "label": "Transition Latency",
      "type": "int",
      "units": "nanoseconds",
      "default": 2000
    },

    "Simple": {
      "cores_per_socket": {
        "label": "Cores Per Socket",
        "type": "int",
        "default": 1
      }
    }
  },

  "OS Emulation": {
    "pthread_replace": {
      "label": "PThread Replace",
      "description": "Whether or not to emulate pthread_mutex/pthread_cond/pthread_barrier functions. When false, user-space code is simulated and SYS_futex is emulated.",
      "type": "boolean",
      "default": false
    },
    "nprocs": {
      "label": "Override get_nprocs",
      "description": "The value the emulated get_nprocs() call returns. If this is 0, it will return the number of cores simulated.",
      "type": "int",
      "default": 0
    },
    "clock_replace": {
      "label": "Simulate Clock Functions",
      "description": "Whether or not to replace gettimeofday() and the like to return a simulated time rather than the host wall time. When false, these functions return the host time.",
      "type": "boolean",
      "default": true
    },
    "time_start": {
      "label": "Simulator Start Time",
      "description": "The 'time zero' for emulated gettimeofday(). When 'Simulate Clock Functions' is toggled on, this value will be the initial time those functions report.",
      "type": "int",
      "default": 1337000000
    }
  },

  "Queue Model": {
    "Basic": {
      "moving_avg_enabled": {
        "label": "Moving Average Enabled",
        "type": "boolean",
        "default": true
      },
      "moving_avg_type": {
        "label": "Moving Average Type",
        "type": ["arithmetic_mean"],
        "default": "arithmetic_mean"
      },
      "moving_avg_window_size": {
        "label": "Moving Average Window Size",
        "type": "int",
        "default": 1024
      }
    },

    "History List": {
      "analytical_model_enabled": {
        "type": "boolean",
        "default": true,
        "label": "Analytical Model Enabled"
      },
      "max_list_size": {
        "label": "Maximum List Size",
        "default": 100,
        "type": "int"
      }
    },

    "Windowed MG1": {
      "window_size": {
        "label": "Window Size",
        "default": 1000,
        "type": "int"
      }
    }
  },

  "Progress Trace": {
    "enabled": {
      "label": "Enabled",
      "type": "boolean",
      "default": false
    },
    "interval": {
      "label": "Interval",
      "type": "int",
      "default": 5000
    }
  },

  "Routine Tracer": {
    "type": {
      "label": "Type",
      "type": ["funcstats"],
      "default": "funcstats"
    }
  },

  "Instruction Tracer": {
    "type": {
      "label": "Type",
      "type": ["fpstats"],
      "default": "fpstats"
    }
  },

  "Loop Tracer": {
    "iter_count": {
      "label": "Iteration Count",
      "type": "int",
      "units": "iterations",
      "default": 36
    },
    "iter_start": {
      "label": "Iteration Start",
      "type": "int",
      "default": 0
    }
  },

  "Sampling": {
    "enabled": {
      "label": "Enabled",
      "type": "boolean",
      "default": false
    }
  },

  "Core": {
    "spin_loop_detection": {
      "type": "boolean",
      "label": "Spin Loop Detection",
      "default": false
    },

    "Hook Periodic Instructions": {
      "ins_global": {
        "label": "Global Instructions",
        "units": "instructions",
        "type": "int",
        "default": 1000000
      },
      "ins_per_core": {
        "label": "Instructions Per Core",
        "units": "instructions",
        "type": "int",
        "default": 10000
      }
    },

    "Light Cache": {
      "num": {
        "label": "Number",
        "type": "int",
        "default": 0
      }
    }
  },

  "Network": {
    "collect_traffic_matrix": {
      "label": "Collect Traffic Matrix",
      "type": "boolean",
      "default": false
    },

    "memory_model_1": {
      "label": "Memory Model 1",
      "type": ["bus", "emesh_hop_counter", "emesh_hop_by_hop", "magic"],
      "default": "bus"
    },
    "memory_model_2": {
      "label": "Memory Model 2",
      "type": ["bus", "emesh_hop_counter", "emesh_hop_by_hop", "magic"],
      "default": "bus"
    },
    "system_model": {
      "label": "System Model",
      "type": ["bus", "emesh_hop_counter", "emesh_hop_by_hop", "magic"],
      "default": "magic"
    },

    "EMESH Hop Counter": {
      "hop_latency": {
        "label": "Hop Latency",
        "type": "int",
        "units": "cycles",
        "default": 2
      },
      "link_bandwidth": {
        "label": "Link Bandwidth",
        "type": "int",
        "units": "bits/cycle",
        "default": 64
      }
    },

    "EMESH Hop by Hop": {
      "link_bandwidth": {
        "label": "Link Bandwidth",
        "type": "int",
        "units": "bits/cycle",
        "default": 64
      },
      "hop_latency": {
        "label": "Hop Latency",
        "type": "int",
        "units": "cycles",
        "default": 2
      },
      "concentration": {
        "label": "Number of Cores Per Network Stop",
        "type": "int",
        "default": 1
      },
      "dimensions": {
        "label": "Dimensions",
        "type": ["Line/Ring", "2D Mesh/Torus"],
        "default": "2D Mesh/Torus"
      },
      "wrap_around": {
        "label": "Use Wrap-around Links",
        "type": "boolean",
        "default": false
      },
      "size": {
        "label": "Size",
        "type": "string",
        "default": ""
      },
      "Queue Model": {
        "enabled": {
          "label": "Enabled",
          "type": "boolean",
          "description": "",
          "default": true
        },
        "type": {
          "label": "Type",
          "type": ["history_list", "contention", "basic", "windowed_mg1"],
          "description": "",
          "default": "history_list"
        }
      },
      "Broadcast Tree": {
        "enabled": {
          "label": "Enabled",
          "type": "boolean",
          "description": "",
          "default": false
        }
      }
    },

    "Bus": {
      "bandwidth": {
        "label": "Bandwidth",
        "type": "float",
        "units": "GB/s",
        "default": 25.6
      },
      "ignore_local_traffic": {
        "label": "Ignore Local Traffic",
        "type": "boolean",
        "default": true
      },
      "Queue Model": {
        "enabled": {
          "label": "Enabled",
          "type": "boolean",
          "description": "",
          "default": true
        },
        "type": {
          "label": "Type",
          "type": ["history_list", "contention", "basic", "windowed_mg1"],
          "description": "",
          "default": "contention"
        }
      }
    }
  }
}
