{
  "Performance Model": {
    "Cache": {
      "levels": {
        "label": "Cache Levels",
        "type": "int",
        "default": 3
      }
    },

    "Core": {
      "core_model": {
        "label": "Core Model",
        "type": ["nehalem"],
        "default": "nehalem"
      },

      "frequency": {
        "label": "Frequency",
        "type": "float",
        "units": "GHz",
        "default": 1.0
      },

      "logical_cpus": {
        "label": "SMT Threads Per Core",
        "type": "int",
        "description": "The number of SMT threads per core.",
        "default": 1
      },

      "type": {
        "label": "Type",
        "type": ["interval", "simple", "magic", "iocoom", "oneipc"],
        "description": "",
        "default": "interval"
      },

      "Interval Timer": {
        "dispatch_width": {
          "label": "Dispatch Width",
          "type": "int",
          "default": 4
        },
        "window_size": {
          "label": "Window Size",
          "type": "int",
          "default": 128
        },
        "num_outstanding_loadstores": {
          "label": "Number of Outstanding Load-stores",
          "type": "int",
          "default": 10
        },
        "memory_dependency_granularity": {
          "type": "int",
          "label": "Memory Dependency Granularity",
          "default": 8
        },
        "lll_cutoff": {
          "label": "Long-Latency Load Cutoff",
          "type": "int",
          "units": "cycles",
          "default": 30,
          "description": "Within an interval simulation model, this parameter specifies the value, in cycles, of how long a memory load would need to be to be considered a long-latency load. This value represents the number of cycles that, if greater, would cause a ROB in an out-of-order core to stall."
        },
        "lll_dependency_granularity": {
          "label": "Long-Latency Load Dependency Granularity",
          "type": "int",
          "units": "bytes",
          "default": 64,
          "description": "Model the MSHR for overlapping misses by adding additional dependencies on long-latency loads using cache-line granularity."
        },
        "issue_contention": {
          "label": "Issue Contention",
          "type": "boolean",
          "default": true
        },
        "issue_memops_at_dispatch": {
          "label": "Issue Memory Operations at Dispatch",
          "type": "boolean",
          "default": false,
          "description": "Whether or not memory operations will be issued to the cache hierarchy at dispatch. Otherwise issue them at fetch."
        }
      }
    },

    "DRAM": {
      "num_controllers": {
        "label": "Number of Controllers",
        "type": "int",
        "default": -1
      },
      "controllers_interleaving": {
        "label": "Controllers Interleaving",
        "type": "int",
        "default": 4
      },
      "latency": {
        "label": "Access Latency",
        "type": "int",
        "units": "nanoseconds",
        "default": 45
      },
      "per_controller_bandwidth": {
        "label": "Bandwidth Per Controller",
        "type": "float",
        "units": "GB/s",
        "default": 7.6
      },
      "chips_per_dimm": {
        "label": "Chips Per DIMM",
        "type": "int",
        "default": 8
      },
      "dimms_per_controller": {
        "label": "DIMMs Per Controller",
        "type": "int",
        "default": 4
      },
      "direct_access": {
        "label": "Direct Access",
        "type": "boolean",
        "default": false
      },
      "type": {
        "label": "Type",
        "type": ["constant"],
        "default": "constant"
      },

      "Cache": {
        "enabled": {
          "label": "Enabled",
          "description": "Whether or not the DRAM cache is enabled.",
          "type": "boolean",
          "default": false
        }
      },

      "Normal": {
        "standard_deviation": {
          "label": "Standard Deviation",
          "type": "int",
          "default": 0
        }
      },

      "Queue Model": {
        "enabled": {
          "label": "Enabled",
          "type": "boolean",
          "default": true
        },

        "type": {
          "label": "Type",
          "type": ["history_list"],
          "default": "history_list"
        }
      }
    },

    "Branch Predictor": {
      "type": {
        "label": "Type",
        "type": ["pentium_m"],
        "default": "pentium_m"
      },
      "mispredict_penalty": {
        "label": "Misprediction Penalty",
        "type": "int",
        "default": 13
      },
      "size": {
        "label": "Size",
        "type": "int",
        "default": 1024
      }
    },

    "DRAM Directory": {
      "total_entries": {
        "label": "Number of Entries",
        "type": "int",
        "default": 1048576,
        "units": "entries"
      },
      "associativity": {
        "label": "Associativity",
        "type": "int",
        "default": 16
      },
      "directory_type": {
        "label": "Directory Type",
        "type": ["full_map"],
        "default": "full_map"
      },
      "directory_cache_access_time": {
        "label": "Directory Cache Access Time",
        "type": "int",
        "default": 10
      },
      "interleaving": {
        "label": "Interleaving",
        "type": "int",
        "default": 1
      },
      "max_hw_sharers": {
        "label": "Max Hardware Sharers",
        "type": "int",
        "default": 64
      },

      "Limitless": {
        "software_trap_penalty": {
          "label": "Software Trap Penalty",
          "type": "int",
          "default": 200
        }
      }
    },

    "TLB": {
      "penalty": {
        "label": "Page Walk Penalty",
        "type": "int",
        "units": "cycles"
      },
      "penalty_parallel": {
        "label": "Penalty Parallel",
        "type": "boolean",
        "default": true
      }
    },

    "D-TLB": {
      "size": {
        "label": "Number of Entries",
        "type": "int",
        "default": 64
      },

      "associativity": {
        "label": "Associativity",
        "type": "int",
        "default": 4
      }
    },

    "I-TLB": {
      "size": {
        "label": "Number of Entries",
        "type": "int",
        "default": 128
      },

      "associativity": {
        "label": "Associativity",
        "type": "int",
        "default": 4
      }
    },

    "S-TLB": {
      "size": {
        "label": "Number of Second-level Entries",
        "type": "int",
        "default": 512
      },

      "associativity": {
        "label": "Associativity",
        "type": "int",
        "default": 4
      }
    },

    "L1 D-Cache": {
      "perfect": {
        "label": "Perfect",
        "type": "boolean",
        "default": false
      },
      "cache_size": {
        "label": "Cache Size",
        "type": "int",
        "default": 32
      },
      "cache_block_size": {
        "label": "Cache Block Size",
        "units": "bytes",
        "type": "int",
        "default": 64
      },
      "associativity": {
        "label": "Associativity",
        "type": "int",
        "default": 8
      },
      "address_hash": {
        "label": "Associativity",
        "type": ["mask", "mod"],
        "default": "mask"
      },
      "replacement_policy": {
        "label": "Replacement Policy",
        "type": ["lru"],
        "default": "lru"
      },
      "data_access_time": {
        "label": "Data Access Time",
        "type": "int",
        "default": 4
      },
      "tags_access_time": {
        "label": "Tags Access Time",
        "type": "int",
        "default": 1
      },
      "writeback_time": {
        "label": "Writeback Time",
        "type": "int",
        "default": 0
      },
      "perf_model_type": {
        "label": "Performance Model Type",
        "type": ["parallel"],
        "default": "parallel"
      },
      "writethrough": {
        "label": "Is Writethrough",
        "type": "boolean",
        "default": false
      },
      "shared_cores": {
        "label": "Shared Cores Per Cache",
        "type": "int",
        "default": 1
      },
      "prefetcher": {
        "label": "Prefetcher",
        "type": ["none"],
        "default": "none"
      },
      "dvfs_domain": {
        "label": "DVFS Domain",
        "type": ["global", "core"],
        "default": "core"
      },
      "outstanding_misses": {
        "label": "Outstanding Misses",
        "type": "int",
        "default": 0
      },
      "next_level_read_bandwidth": {
        "label": "Next Level Read Bandwidth",
        "type": "int",
        "default": 0
      }
    },

    "Fast Forward": {
      "model": {
        "type": ["none"],
        "label": "Model",
        "default": "none"
      },

      "One-IPC": {
        "include_branch_misprediction": {
          "type": "boolean",
          "label": "Include Branch Misprediction",
          "default": false
        },
        "include_memory_latency": {
          "type": "boolean",
          "label": "Include Memory Latency",
          "default": false
        },
        "interval": {
          "type": "int",
          "label": "Internal",
          "default": 100000
        }
      }
    },

    "L1 I-Cache": {
      "perfect": {
        "label": "Perfect",
        "type": "boolean",
        "default": false
      },
      "cache_size": {
        "label": "Cache Size",
        "type": "int",
        "default": 32
      },
      "cache_block_size": {
        "label": "Cache Block Size",
        "units": "bytes",
        "type": "int",
        "default": 64
      },
      "associativity": {
        "label": "Associativity",
        "type": "int",
        "default": 4
      },
      "address_hash": {
        "label": "Associativity",
        "type": ["mask", "mod"],
        "default": "mask"
      },
      "replacement_policy": {
        "label": "Replacement Policy",
        "type": ["lru"],
        "default": "lru"
      },
      "data_access_time": {
        "label": "Data Access Time",
        "type": "int",
        "default": 4
      },
      "tags_access_time": {
        "label": "Tags Access Time",
        "type": "int",
        "default": 1
      },
      "writeback_time": {
        "label": "Writeback Time",
        "type": "int",
        "default": 0
      },
      "perf_model_type": {
        "label": "Performance Model Type",
        "type": ["parallel"],
        "default": "parallel"
      },
      "writethrough": {
        "label": "Is Writethrough",
        "type": "boolean",
        "default": false
      },
      "shared_cores": {
        "label": "Shared Cores Per Cache",
        "type": "int",
        "default": 1
      },
      "prefetcher": {
        "label": "Prefetcher",
        "type": ["none"],
        "default": "none"
      },
      "dvfs_domain": {
        "label": "DVFS Domain",
        "type": ["global", "core"],
        "default": "core"
      },
      "outstanding_misses": {
        "label": "Outstanding Misses",
        "type": "int",
        "default": 0
      },
      "next_level_read_bandwidth": {
        "label": "Next Level Read Bandwidth",
        "type": "int",
        "default": 0
      },
      "coherent": {
        "label": "Coherent",
        "type": "boolean",
        "default": true
      }
    },

    "L2 Cache": {
      "perfect": {
        "label": "Perfect",
        "type": "boolean",
        "default": false
      },
      "cache_size": {
        "label": "Cache Size",
        "type": "int",
        "default": 256
      },
      "cache_block_size": {
        "label": "Cache Block Size",
        "units": "bytes",
        "type": "int",
        "default": 64
      },
      "associativity": {
        "label": "Associativity",
        "type": "int",
        "default": 8
      },
      "address_hash": {
        "label": "Associativity",
        "type": ["mask", "mod"],
        "default": "mask"
      },
      "replacement_policy": {
        "label": "Replacement Policy",
        "type": ["lru"],
        "default": "lru"
      },
      "data_access_time": {
        "label": "Data Access Time",
        "type": "int",
        "default": 8
      },
      "tags_access_time": {
        "label": "Tags Access Time",
        "type": "int",
        "default": 3
      },
      "writeback_time": {
        "label": "Writeback Time",
        "type": "int",
        "default": 50
      },
      "perf_model_type": {
        "label": "Performance Model Type",
        "type": ["parallel"],
        "default": "parallel"
      },
      "writethrough": {
        "label": "Is Writethrough",
        "type": "boolean",
        "default": false
      },
      "shared_cores": {
        "label": "Shared Cores Per Cache",
        "type": "int",
        "default": 1
      },
      "prefetcher": {
        "label": "Prefetcher",
        "type": ["none"],
        "default": "none"
      },
      "dvfs_domain": {
        "label": "DVFS Domain",
        "type": ["global", "core"],
        "default": "core"
      },
      "next_level_read_bandwidth": {
        "label": "Next Level Read Bandwidth",
        "type": "int",
        "default": 0
      }
    },

    "L3 Cache": {
      "perfect": {
        "label": "Perfect",
        "type": "boolean",
        "default": false
      },
      "cache_size": {
        "label": "Cache Size",
        "type": "int",
        "default": 8192
      },
      "associativity": {
        "label": "Associativity",
        "type": "int",
        "default": 16
      },
      "address_hash": {
        "label": "Associativity",
        "type": ["mask", "mod"],
        "default": "mask"
      },
      "replacement_policy": {
        "label": "Replacement Policy",
        "type": ["lru"],
        "default": "lru"
      },
      "data_access_time": {
        "label": "Data Access Time",
        "type": "int",
        "default": 30
      },
      "tags_access_time": {
        "label": "Tags Access Time",
        "type": "int",
        "default": 10
      },
      "writeback_time": {
        "label": "Writeback Time",
        "type": "int",
        "default": 0
      },
      "perf_model_type": {
        "label": "Performance Model Type",
        "type": ["parallel"],
        "default": "parallel"
      },
      "writethrough": {
        "label": "Is Writethrough",
        "type": "boolean",
        "default": false
      },
      "shared_cores": {
        "label": "Shared Cores Per Cache",
        "type": "int",
        "default": 4
      },
      "prefetcher": {
        "label": "Prefetcher",
        "type": ["none"],
        "default": "none"
      },
      "dvfs_domain": {
        "label": "DVFS Domain",
        "type": ["global", "core"],
        "default": "global"
      }
    },

    "NUCA": {
      "enabled": {
        "label": "Enable NUCA",
        "type": "boolean",
        "description": "",
        "default": false
      }
    },

    "Sync": {
      "reschedule_cost": {
        "label": "Synchronization Reschedule Cost",
        "type": "int",
        "description": "Allows one to add an additional latency to pthread mutex calls when high single-mutex contention causes the kernel to wait for long periods before returning that would otherwise not exist with uncontended or low-contention mutexes.",
        "units": "nanoseconds",
        "default": 0
      }
    },

    "Last-Level Cache": {
      "evict_buffers": {
        "type": "int",
        "label": "Eviction Buffers",
        "default": 8
      }
    }
  },

  "Caching Protocol": {
    "type": {
      "label": "Caching Protocol Type",
      "type": ["parametric_dram_directory_msi"],
      "default": "parametric_dram_directory_msi"
    },
    "variant": {
      "label": "Variant",
      "type": ["mesi"],
      "default": "mesi"
    }
  },

  "Power": {
    "vdd": {
      "label": "V DD",
      "type": "float",
      "default": 1.2,
      "units": "volts"
    },

    "technology_node": {
      "label": "Technology Node",
      "type": "int",
      "default": 45,
      "units": "nm"
    }
  },

  "Clock Skew Minimization": {
    "report": {
      "type": "boolean",
      "label": "Report",
      "default": false
    },

    "scheme": {
      "label": "Scheme",
      "type": ["barrier"],
      "default": "barrier"
    },

    "Barrier": {
      "quantum": {
        "label": "Quantum",
        "type": "int",
        "default": 100
      }
    }
  }
}
