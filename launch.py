import os
import time
import json
import subprocess

from occam import Occam

# Gather paths
scripts_path   = os.path.dirname(__file__)
simulator_path = "%s/package" % (scripts_path)
job_path       = os.getcwd()

object = Occam.load()

# Open object.json for command line options
config = object.configuration("General Options")
cpu    = object.configuration("CPU Options")

# Gather object information
cores = 2

if "General" in config:
  if "total_cores" in config["General"]:
    cores = config["General"]["total_cores"]

# Generate input
input_file = open('input.cfg', 'w')

# Reformatted options
options = {}

def write_dict(title, d):
  # Write [header]
  if not title == None:
    input_file.write("[%s]\n" % (title))

  for key, value in d.items():
    if isinstance(value, dict):
      ""
    elif isinstance(value, (bool)):
      if value == True:
        input_file.write('%s = "true"\n' % (key))
      else:
        input_file.write('%s = "false"\n' % (key))
    elif isinstance(value, (int, long, float)):
      input_file.write("%s = %s\n" % (key, value))
    else:
      input_file.write('%s = "%s"\n' % (key, value))

  for key, value in d.items():
    if isinstance(value, dict):
      write_dict(key, value)

def out_dict(title, d, to):
  # Write [header]
  if not title is None:
    to[title] = {}
    to = to[title]

  # Write items
  for k, v in d.items():
    if not isinstance(v, dict):
      key = k
      value = v

      # Rewrite values if needed:

      # Special case for network/emesh_hop_by_hop/dimensions
      if key == "dimensions":
        if v == "Line/Ring":
          value = 1
        elif v == "2D Mesh/Torus":
          value = 2
        else:
          value = 0
      elif key == "writethrough":
        value = (v == 1)

      # Rewrite and redo keys if needed:

      # Common case: just write the value
      to[key] = value

  # Write sub-items
  for k, v in d.items():
    if isinstance(v, dict):
      if k == "Hook Periodic Instructions":
        k = "hook_periodic_ins"
      elif k == "OS Emulation":
        k = "osemu"
      elif k == "Performance Model":
        k = "perf_model"
      elif k == "Last-Level Cache":
        k = "llc"

      if k == "Global":
        # Special case for global (they go to global space and not
        #                          under a header)
        out_dict(None, v, to)
      else:
        new_title = k.lower().replace(' ', '_').replace('-', '')
        if not title is None:
          new_title = "%s/%s" % (title, new_title)

        out_dict(new_title, v, to)

# Read general configuration
out_dict(None, config, options)

# Read CPU config
out_dict(None, cpu, options)

# Write out configuration
write_dict(None, options)

inputs = object.inputs("trace")

# Look for runtime instructions. Do we 'fifo' the trace?
if (inputs) > 0:
  # Fifo!
  fifo = True
else:
  fifo = False

if not fifo:
  # Run test case of "fft"
  binary = "%s/run-fft.sh" % (scripts_path)

  # Form arguments
  args = ["/bin/sh",
          binary,
          simulator_path,
          str(cores)]
else:
  # Wait for fifo files
  # For every benchmark, look for a fifo for the first thread
  trace = object.inputs('trace')[0]
  trace_generators = trace.inputs('trace-generator')

  app = 0
  for trace_generator in trace_generators:
    # For every benchmark in the trace generator (should only be one)
    benchmarks = trace_generator.inputs()
    for benchmark in benchmarks:
      for type in ['', '_response']:
        filename = "%s/foo%s.app%s.th0.sift" % (trace.volume(), type, app)
        while not os.path.exists(filename):
          time.sleep(1)
      app = app + 1

    if len(benchmark) == 0:
      for type in ['', '_response']:
        filename = "%s/foo%s.app%s.th0.sift" % (trace.volume(), type, app)
        while not os.path.exists(filename):
          time.sleep(1)
      app = app + 1

  # At this point, all fifos are generated, and we can now start

  # Run trace through fifo
  binary = "%s/run-sniper" % (simulator_path)

  # Form arguments for sniper
  args = [binary,
          "-n",
          str(cores),
          "-m",
          "'localhost'",
          "-d",
          job_path,
          "--curdir",
          job_path,
          "--trace-manual",
          "-g",
          "--traceinput/enabled=true",
          "-g",
          "--traceinput/emulate_syscalls=true",
          "-g",
          "--traceinput/num_apps=%s" % (app),
          "-g",
          "--traceinput/trace_prefix=%s/foo" % (trace.volume())]

# Run
command = ' '.join(args)

# Form command to gather results
finish_command = "python %s/parse.py" % (scripts_path)

# Tell OCCAM how to run DRAMSim2
Occam.report(command, finish_command)
