import os
from occam import Occam

object = Occam.load()

# Gather paths
scripts_path   = os.path.dirname(__file__)
simulator_path = "%s/package" % (scripts_path)

# Look for runtime instructions. Do we 'fifo' the trace?

# We are always fifo'ing!
# So, fifo, already...

# Create the trace nodes and record them

# We need to set up the benchmark input in the trace path
binary = "%s/record-trace" % (simulator_path)

# Pull in benchmark arguments
command = ""

outputs = object.outputs("trace")
benchmarks = object.inputs()

if len(outputs) > 0:
  print(outputs)
  trace = outputs[0]
  print(trace)

  benchmark_index = 0
  for benchmark in benchmarks:
    command = command + benchmark.command() + " "

    # For every benchmark in the trace generator (should only be one)
    # Create a fifo for the first thread
    for type in ('', '_response'):
      filename = "%s/foo%s.app%s.th0.sift" % (trace.volume(), type, benchmark_index)
      if not os.path.exists(filename):
        os.mkfifo(filename)
      benchmark_index += 1

  if len(benchmarks) == 0 or len(outputs) == 0:
    # Run test case of "fft"
    for type in ('', '_response'):
      filename = "%s/foo%s.app%s.th0.sift" % (trace.volume(), type, str(0))
      print("mkfifo %s" % (filename))
      if not os.path.exists(filename):
        os.mkfifo(filename)
    os.system("ls %s" % (trace.volume()))
    cores = 2
    command = "%s/run-fft.sh %s" % (scripts_path, str(cores))

  # Form arguments
  args = [binary,
        "--routine-tracing",
        "-o",
        "%s/foo" % (trace.volume()),
        "-e",
        "1",
        "-r",
        "1",
        "-s",
        str(object.index() or 0),
        "--roi",
        "--"]

  # Prepend trace generator arguments
  command = ' '.join(args) + " " + command

# Run
Occam.report(command)
