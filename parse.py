# Sniper's 'sim.out' file looks like this:
#                                     | Core 0     | Core 1
#   Key                               | value      | value
# Category                            |            |
#   Key                               | value      | value
# Category                            |            |
#   Category                          |            |
#     Key                             | value      | value

import re
import json
import os
from occam import Occam

object = Occam.load()

f = open('sim.out', 'r')

# Read header (for number of cores)
header = f.readline()

# Count '|' characters
cores = header.count('|')

# Start with a data hash with each core.
data = {
  'data': {
    "Cores": [{} for _ in range(cores)]
  }
}

# Establish each core with empty global hash
data['data']["Cores"] = [{'Global': {}} for x in data['data']["Cores"]]

# Start looking for "  Key"
categories = [[x] for x in data['data']['Cores']]
current_indent = 1
current_category = [x['Global'] for x in data['data']['Cores']]

for line in f:
  # When we see an indent level less than our current indent, deal with that.
  m = re.search('^(\s*)([^|]+)\s*[|](.*)$', line)
  indent = m.group(1).count(' ')/2
  key = m.group(2).strip()
  value = m.group(3)
  values = [x.strip() for x in value.split('|')]

  # Go up categories
  if indent < current_indent:
    while indent < current_indent:
      current_indent -= 1
      current_category = [x.pop() for x in categories]

  if values[0] == '':
    # Category
    current_indent += 1
    [x.update({key: {}}) for x in current_category]
    [x.append(current_category[i]) for i,x in enumerate(categories)]
    current_category = [x[key] for x in current_category]
  else:
    # Value
    [x.update({key: values[i]}) for i,x in enumerate(current_category)]

f.close()

o = open(os.path.join(object.path(), "output.json"), "w+")

o.write(json.dumps(data))

o.close()

print(json.dumps(data, indent=4, sort_keys=True, separators=(',', ': ')))
