pyenv local 2.7.6

ROOT=$(readlink -f $(dirname $0)/package)
${ROOT}/run-sniper -c input.cfg --roi --viz --viz-aso -- ${ROOT}/test/fft/fft -p ${2}
